# rs-url-parser-ui

UI example for rs-url-parser npm implementation

## Index

* [Requirements](#requirements)
* [Download & Install](#download-and-install)
* [Usage](#usage)

## Requirements
 - Node version 14.17.4
 - Npm version 6.14.14

## Download and Install

Use the following command to install RS Url Parser

```bash
$ git clone https://gitlab.com/Rivet/rs-url-parser-ui.git
$ cd rs-url-parser-ui
$ npm install
```

## Usage

### Build
```bash
$ npm run build
```

### UI

Open index.html

You will see a form with two input fields: format and instance. When submitting, it will
use the `rs-url-parser` npm to process the url variable parts and match their instance values.

Example:
- Format input: '/:version/api/:collection/:id'
- Instance input: '/6/api/listings/3?sort=desc&limit=10'

Results:

```json
    {
        "version": "6",
        "collection": "listings",
        "id": "3",
        "sort": "desc",
        "limit": "10"
    }
```