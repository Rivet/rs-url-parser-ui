import { urlParser } from 'rs-url-parser';

export default class UrlParserForm extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: "open" });
    }

    get instanceDefault() {
        return this.getAttribute("instance-default");
    }

    get formatDefault() {
        return this.getAttribute("format-default");
    }

    get title() {
        return this.getAttribute("title");
    }

    connectedCallback() {
        this.render();
        let form = this.shadowRoot.querySelector("#url-parser-form");
        form.addEventListener("submit", e => this.submitFormHandler(e));
    }

    render() {
        const template = this.createTemplate();
        this.shadowRoot.appendChild(template.content.cloneNode(true));
    }

    disconnectedCallback() {
        let form = this.shadowRoot.querySelector("#url-parser-form");
        form.removeEventListener("submit");
    }

    resetForm(ele) {
        ele.hidden = true;
    }

    submitFormHandler(e) {
        e.preventDefault();
        let parsed = this.shadowRoot.querySelector("#parsed");
        let format = this.shadowRoot.querySelector("#url-format");
        let instance = this.shadowRoot.querySelector("#url-instance");
        this.resetForm(parsed);
        this.parseUrl(format.value, instance.value);
    }

    parseUrl(format, instance) {
        let form = this.shadowRoot.querySelector("form")
        let inputs = form.querySelectorAll("input");
        if (!this.validate(inputs)) { return; }

        let result = urlParser(format, instance);

        let parsed = this.shadowRoot.querySelector("#parsed");
        parsed.hidden = false;
        parsed.innerHTML = JSON.stringify(result, undefined, 3);
    }

    validate(inputs) {
        let valid = true;
        inputs.forEach(e => {
            if (!e.value || !e.checkValidity()) {
                valid = false;
            }
        });
        return valid;
    }

    createTemplate() {
        const template = document.createElement('template');
        template.innerHTML = `
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        <style>
        input {
            display: block;
            width: 100%;
            margin-bottom: 1em;
        }

        form {
            padding: .6em;
            text-align: left;
        }

        input[type=text] {
            border-color: #f3f3f3;
            background-color: #f3f3f3;
        }

        button[type=submit] {
            margin: 0 auto;
        }

        code {
            margin: 1em auto;
            text-align: left;
            background-color: #f3f3f3;
        }

        pre {
            padding: .8em;
            margin: 0;
        }

        #error {
            max-width: 600px;
            margin: 0 auto;
        }
        </style>
        <div class="container">
        <h1>${this.title}</h1>
        <div class="row row-cols-1 align-items-stretch g-4 py-5">
            <div class="col mx-auto">
                <div class="card card-cover h-100 overflow-hidden rounded-5 shadow-lg">
                    <form action="#" id="url-parser-form">
                        <div class="form-floating">
                            <input type="text" name="url_format" id="url-format" class="form-control" required value="${this.formatDefault}" placeholder="/:version/api/:collection/:id"/>
                            <label for="url-format">URL Format</label>
                        </div>
                        <div class="form-floating">
                            <input type="text" name="url_format" id="url-instance" class="form-control" required value="${this.instanceDefault}" placeholder="/6/api/listings/3?sort=desc&limit=10"/>
                            <label for="url-instance">URL Instance</label>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary my-2">Parse Url</button>
                        </div>
                    </form>
                    <div class="alert alert-danger" role="alert" id="error" hidden></div>
                    <code>
                        <pre id="parsed" hidden></pre>
                    </code>
                </div>
            </div>
        </div>
        </div>`;
        return template;
    }
}
