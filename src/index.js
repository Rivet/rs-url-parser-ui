import HeaderMenu from './components/header.js';
import UrlParserForm from './components/url-parser.js';

window.customElements.define("header-menu", HeaderMenu);
window.customElements.define("url-parser-form", UrlParserForm);
