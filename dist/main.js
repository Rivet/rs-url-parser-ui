/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/rs-url-parser/index.js":
/*!*********************************************!*\
  !*** ./node_modules/rs-url-parser/index.js ***!
  \*********************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("\n\nmodule.exports = __webpack_require__(/*! ./src/parser */ \"./node_modules/rs-url-parser/src/parser.js\");\n\n//# sourceURL=webpack://rs-url-parser-front/./node_modules/rs-url-parser/index.js?");

/***/ }),

/***/ "./node_modules/rs-url-parser/src/parser.js":
/*!**************************************************!*\
  !*** ./node_modules/rs-url-parser/src/parser.js ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"urlParser\": () => (/* binding */ urlParser)\n/* harmony export */ });\n\n/**\n * RS Exercise - URL Parser\n * @param {string} format - URL Format. For example: '/:version/api/:collection/:id'\n * @param {string} instance - URL Instance. For example: '/6/api/listings/3?sort=desc&limit=10'\n * @param {Boolean} [protocol] - Defines if URL Format and Instance have protocol section. False as default.\n * @param {Boolean} [domain] - Defines if URL Format and Instance have domain section. False as default.\n */\n\n function urlParser(format, instance, protocol = false, domain = false) {\n\n    // TO-DO: Validations\n    // TO-DO: Implement clearProtocol()\n    // TO-DO: Implement clearDomain()\n    \n    const formatArr = format.split('/').filter(i => i);\n    const instancePathArr = instance.split(\"?\")[0].split('/').filter(i => i);\n    const instanceSearch = instance.split(\"?\")[1] ? instance.split(\"?\")[1].replace('?',\"\") : undefined;\n    let result = {};\n\n    formatArr.forEach((e, i) => {\n        if (e.slice(0,1) == \":\") {\n            result[e.slice(1,e.length)] = instancePathArr[i];\n        }\n    })\n\n    if (instanceSearch) {\n        Object.assign(result, Object.fromEntries(new Map(instanceSearch.split(\"&\").map(p => p.split(\"=\")))));\n    }\n\n    return result;\n}\n\n\n//# sourceURL=webpack://rs-url-parser-front/./node_modules/rs-url-parser/src/parser.js?");

/***/ }),

/***/ "./src/components/header.js":
/*!**********************************!*\
  !*** ./src/components/header.js ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ HeaderMenu)\n/* harmony export */ });\nclass HeaderMenu extends HTMLElement {\r\n    constructor() {\r\n        super();\r\n        this.attachShadow({ mode: \"open\" });\r\n        this.shadowRoot.innerHTML = \r\n        `\r\n        <style>\r\n        nav {\r\n            position: fixed;\r\n            top: 0;\r\n            left: 0;\r\n            display: flex;\r\n            justify-content: space-between;\r\n            align-items: center;\r\n            width: 100%;\r\n            padding: .5em;\r\n            box-sizing: border-box;\r\n        }\r\n        \r\n        #nav-logo {\r\n            background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQwAAAAiCAYAAACqXedhAAAAAXNSR0IArs4c6QAAHbVJREFUeAHtXQt8VMW5nznn5EHC22wCgWR3UVHAJ3p9tQqCVcRXVQQVWnwgKEHysr2+bkWttaJJeCtWq7RoEatiL0oRy8Mq1BeUFpAqZjckPPIgvBKy2d1z5v6/szubk5PdvIjecu/O77eZ1zffzHwz8833fTNzwlk7Xeac0tOChjLaEMYlnPOTmWAuwURfxrjGODvEmTiAsAfoNnPGPxNJKWuqc9Lr2ok+DhanQJwCJwAFeGttPGXegZ6HA0fuY0zcJRgbLGE5Z9VgDruEYNUI62AcPZB3OpjIQAtMIxf8bZWrxfsKsr+Q6XE/ToE4BU5cCkRlGMOWi8TKcu90SA2PgFGkQWLYLzhboSpiVWq33htKp/U9HK3LxGCO6EeHocxwIcRNYCAjwWwUxvm6BM5z9uW7vopWLp4Wp0CcAicGBVowjH4lHlfQYH/EQj8PjOJbrvDHslKcy7+cxgP2LjmKPZPBGOaricrZlfe7SB1p5vrNKxuqB/UnBOM3gfn4ocr8Ynqe67lZnBvNAOOROAXiFDghKKBYWwlmMRLMYjPj4izOlUfSs1xDq/Jdr0VjFlQOzCIRXg8joKpWPDK8f6ZzR3XBoHGQMM4Xgm8zDPHMomLv0pHrhCZh4n6cAnEKnDgUiCzcjDmeC3WdrYQBs5Er7IokQ/18+3ju74quZAxwbjt4cN9l/nrfYkOISds3e6je8V2BO46jcxQg9bEuWJdlluZG/f58t7dzmOKl/j9RwGQYjoVV3Y3G+tdhvEzQFO1yYRin+lhwIggxrSuIUbNn90Bd6IvVbikT9WMN3VHPLenFnnurCtwvdAR/Wom3v6byPtHKcEWvP22Ye8/6y3kwWn5XpWXO3ZsdED4XJLBgVa5rY1fh/b7xHA4eHgOj9RtULxfsY3iXft9tiNd34lHAVElEY/2zUC8GMUV5aH9e9uc6E7Ohb6S21R2FKwkEk6AEoqokzcoLdoU41nBjz+697sDJyi4YRosdxeWnNINpKyLErGBA3x7tF2hk3m2bvY1pxZ4KR1Hpb8gW0xa6zuQHdN8XQmcbhC4e6Ez5eJk4BU5kCmjpcyozDL1+CgySG6rynCU8v2V3blku1I/2es83dDYK29EPYMQcBGupC3cyuhG0X+c7YAD1IriTC7FTcP4PpVu3VZX39auifKujE5Z+Rd6pQWas5Tx4P/JyrfnHF8aJjGADwIymBIWY7Cgpu7063wkDbte4MK0cXYMtjiVOgROPAhozGiah2RoYxjz8sNaaXNb83Zm+oD5jXbl3MkycmWaOYEGAeSHIbkD8AMoEzQtcgp8EmPNx/DEW0gmD6mGAiazmilIiFF7KLOci+wtd69KKPDtR2aRT5omf75rJG5tqjR2i81kDHMF0nAUQ/LuEBgPrhhwn4nQnBE1hCUIYLw4sLt9UUZC1x0w77j++s44bRRxBnAInMAU0qCLXw9B5xDHAuTIiDkCEwJHqaF8gWIq1n0THq0zhzyC8ypnq2hjr1IToQKqAwfgPgPdm/K4Run4117nHTiMs8FeR/+ujetkPkLfWnh8tDmYR5hZgV4zVVhcOusAK53rFk1xXy4rBLXDZjJzo42f6rQgUmVHbHzL81QfrBxuq6AWMlUPOytoZzQbimOc9F9fTJgjdOCeCQrBzHMWlC2Vc1dSFdCpE8Yzi8jOFCJjqXiJP+BoMq0HCST+jpGwQxKEkiicxsc+b7z5E4exFZX2CupGhBxV/Zb6zlNIyF+7JCgb8Q3HSNADH00kYi60j+js/fXM81ym/qx1JlOsrvD8Cja8EwU8H/t6gZa0QyldMEZtn5LnemBU/Gu9qsp8Q+DTBhYsJ/o08EUmbW3Ye143eWMwpWETbmao8mJOb/b6cIDVtdCtsbfcC7DUyEAZ1/6MGM+4gwQCXv0biSPXl0KLk/0QKMwRzt4Gy3dneO90+SBSFMNhOBW8x7SqoYYgdQfqc8rMMPfjkocDhMchLZGEz6bYt3lpHied3iQkJv9wzY+ABWY4H+DD04T9lnHxIVW78mS7TdF2sRNhkGAYLfI7umsygkQfPRXpEEorAC+NPuhDDKK4zfi+8xRSGYDYZ2EsYMwwwlatgLJ7pb/Rfi/5g/aJWAgLRIPXtcMwtm1ad6ySDZQtHTCbgC/wMwOeC/w8Do8E9GtzO5eyP3fuw5+sPmphalKOj9XUV3qWoj1Q7mzOuIUlxQYk3t3+J986OXMQ7b7FIaG2jsVXULEpG+fgzg2Yk+V+LaJiBmbBLbKUW4BTiGjCLNzExFRhAC884xzmPFvesvM61b29u5m6UnJo+b/czRkB/A0zo1m1bPKmQBMbXHxJ7afIJboRUnXZU0UwliQFPuzlUoSpM9v4EAt9pBU0r8Uw1jOAi5LQ01ArRFzJMnt8fmACp4prqma4tVNZQ2SGs6nIEM/CjuyeQcLgPuL0UNp1QIu9mkN7sfosE6ZgvFN3Q14SRRykqhoqgvjJtQcUFNTMGfm0FgLqXF/D5fwn2EjZchxkNE+kgyCV1tXwqDNYvgZlai5lhXdH2sGBgQIsMa4IQF0IPfQ/q5JDW1EnHQk8/4WO/wPy6pqzem4VxqQUj+gpq7FqRlPpsLCZgSjjl3hwQ+SqM03nCV5eRVlQK+vMvIZW9U1U46HfW5sgwSZj1tWybjMfyQY3aGkindKO5Cow3Fpw1HYy2rKbAPRprZAo3xIOqqt5GBwRWGBnOmFs22tCN3yucT6rMd62V6Va/35zd/6Hr+h9Am4dxV2m5Nc8aTi/2voj2TgSu62LhkvDpJZ6fC4NNlXHpo+1+bBilmJZbElIT5+6dltls349VTpaXPq5bFOHhGD+CratnqJP6W9iN6rmq3lCDnWu9hDxOv2pm9rfY+S/1scCLmLCTMKgrFIXPxmkMcYzIQmurGqtKEgsWTImDAHgUF3KYnHtlGEbQccLQzZ1cppG6BYKWYyKTJADVBE0Soj8sM6sHzKsYvmfmwIqaPCdJDyuhgjwFhvIwwWAyf4oJNNIM2/+AiPak44hD/uErYTf6EgOehLZdTYsojK8XDwRI/YqYqjOKvbfrwigBZS2OG5gwNWhWGsqCmYnTQcunLQCRYHVu1jdgODvQgSTopX9CmVKoIdWwbjkNg01G2aEEjEnsPhzw0lbyTKSwJRBmFl8CR2/gWINxmI2F5kC7zoOA9ChvrL8no8hzU2Wh+2+WYixjvte9vsLze8BBVeXrMD6vgP16YEw/E7VehrJLcBJ2k6Yl37N/Zv9qa1m/P0kRzHcy6toM2GZ4rXByzjkczKiqEKuteWgr3kSJUWg3GcurZJ4iuFmXqrG/6H7xG0h+tyMvKsPACdoEmkPARepwVIaBzWAiaOjUtG7rZB12n64RGAZJnFzDeD0QC5csZwgaX+NkqKyLQa+Iuoo3Xck4iIBEbDzor/Pl4tDhBrIj2suhzwtQV6SczI/4XNmFexhiMxYBuJ2xHIODzVQbWZmbBXWha11Yj/8JdgrMPTERA38q1aBxZu7i7amtPRKGo8Q7AbPZVAdMnAbDQmPstJerexw4XBdhFiDON2CW46vz3aa6QIzGUezNBcfHkTIZTJnDH/TPQdFxJh78AdH9mEwy2sIuITNAdFqUTdFOhjAePpVrI/YXZH0mUcwS4rGFJd5VaO+VlCYMgYUVcrRI9UYxX8bRjoOKwh7pqbp+S5KAq8TTu14oMHIbz6EbTTRqKhAKKfyK6nzXPnsy1Mni7Vu8H6LuEZQHdfYmeFEZBvOJJ5HXQ+HqmdIWQ2XI9S/efX5ABPOYI6XZPDPb5xcbwW5VpqrXhRl1qBD+0hhBTSvA6dyv9KBvDdpzPknAEQAZ4Gx1df6gEGOXaVH8cNkcaxZU0jtAm1Gqos2OJkHQEwhHkWcjdvLxGItCqapLHCHpyHMj5hbQGD9G/D67rQnllAXF3lvA2D6wMz2Jh3ww+fvxx4/Q4zDgP9WvuGzY/gLnditMtHDGANdMaWKw5pPdDAzoQ50Zy7AeTvnX3Y6j1vz0ge7CaOWsMIoilE1I6IXR6IudNq+yoOuZhbXCxO7JU7F7bcPgD0K6X5zULSqXtpaJFsZopEBcK5Q/qBqPQeR9A7BLm+D5wb69u79CcTCLKdRHCmOgjiVqiaMkswiniZpC9xxwY+LkpsMOcRPteDJO22ok3FoAK7217HbngZlamQWVowkKxrmkCQdPlWHu49NlH2nCgineXpXvfl6qDWRYrS5wLeCKep0sE82vicIsCI4WGE69nmoqw2Peo0HlF4POm+zMgsrS62WoBJMqf9qvvgkX7DeCzwaB04WqXm1nFgQHfKKqwFUEdTkH8+fsHX/3FljLf19htOM1sK9M2HIutdf50Z6yEehDGmg/F3PUQXE7TKicyASe1+15Mp7xu/2pYEqwbfHfQo2Yg87XYqEXyvzO+OZYcPEg0fjQkWMXdgaHEtTYn8OT65uafPeLnUHSkTLQn44B/gkqg4n/lnpUj6gPbeGxqSQ9sNM8J38wBIJxi/FYMCrhwZqtx58bI1wUJzYSPxjBQlI1ZNzqDxvuWoRyu8007GgiKMZY89sVRrkInIqp01knRF20ojimrpbpWEMJMgx70CgZRqUfVRe4/yzjVh/3bdagj/9tTbOHSUU1mXCJZwl21GVQx+aDIeeCxpBKww4MmE6aZNTqQ4w8gPhgsitY02OFHXPLT8X4TsGu+jzUYVMqjAVbned8GeO7EQvyMdhRYktKsRAcZ7qWmkg2hyC2jwl2VLohxoER7EtISXoEND5CcTsM4rfS/OQnJa+IkmcmGQeO3YXyPZUEPofWDGbRYtB+Iqkpscq0Jx2mgDKCw7pp3U4VA5mi6ewcLK+lIP6p6UVlEFe/WwdbRjcwqEdQS5Br6q/8vsaOL8i2msghZSSzU2Bj2GABHSzDUHQ/lWG7b+6i1vyw6mSHayPeUkxuo0BHsjF5LNZKHmEYGEfLJBCftIpT8Ki6NUlUYBCf4JXxhyYTNsRPIVhBJ2czMGHn4NnA+1a8dcwfdcGCYb0OeFd9rVgDm8MIcHPFWs4e5oZ+AZg9iUVv2vPscZI0kEZG9JRDetkwe/53HSejIdrwAZoxzvqQkvqINFLT3gxtjMq7oMON1r4TPJSVcdhS3rVLWLLdpNYwg+eDeb4jX4Ena+oCMBAMPdSU43CwQ11OxYWqbOsMGjKmTFeUlNFCP/Yj7FCLcaz6VVscvjMVmY3EhIAK8Qom4NmQLh7H69XTYaDJbC8+lLFc3OK1GJ1rZVlMnmL8LqI4BulIdY57v8wzfc57YEKaQaEakR26GYyMcFYplQ+szFSZDPtGu5QSjCtdZjN3Ypie0JxOutDCaLUwehRhGGhdogW4FRsL0UjsCVGjqQQx88ZA8G20HZsIXKj+DYD9Fis5BfBO5F2AnCYpwwRs+aeq0L0Y9qQ00Pxh/NZDDK8FI1rDFfFeokh4DzatWmspTGSzzmSmNbNrWGGsYSy4rTRGGJKzkQ4jZ5MDHS4iy39TSvPQiAGuIrtdoTlE2zHMxddwLD52xxYPSXUfUImFcz0/RN0ZisZJNYYIzZfj7s5PKB2xjyhpx1bvaDQ7DfR8jeLR3Ppyz02AceMka6LML78/e29asXcZbFb3Ql15KhazkfB2nxjV9i1lk2ELeRzM7n0cx7eQ4qoqvO/gWYVlM2rCgs9cvE/qLY5VxTBhHBvLNX4zOrea6fra9Dll40yxtQn+uEN0lp5e4l2CCQcOzFf0HOJ6+tBO71bM3D+0F7lVJcEkDqADZH8xHSYj4TEZBoh9Pbj0TOukwMSio9beBIyLZEPgmQNI8RZOsNNkGqzjlTLcbh+KjGQ4GgwG7S7XCUD0K8IwsP1UYcKaDJgzJbtVdIoyGFc9moH4hXFzhFkwdjSBqaPsX0sLnX6Ifc0KxojAcPoUrtO/JMSxG0CPscA9Fnr5BJyWHU0vKX2iKn/Qc7IotoJktJ0layntks5UhfnwuhqG1yaGKXFhTl8KtTOmjr5lP5sL2NinARFErQTSkt/l1Q31aDKdhJgMg+kK1A+jvHKmcxPPxRl8ZvYHlRVeHMlzUkvM+Ub9xw5SM2y484P1MdCjTw9g09mET0tE5jeBgn/TpvgTU11hbH6M4qyywvMJDhfQtJBDfd22b/a6kdANzOL3vbSeM6PumDSXYmxS2OvNeRzaKaCL4eXlmPSSsjE4dlxlGPp7aSWlT/ZW3bOlwUxW3hk/Y+7uM/TG+mXEnNDg5dmprkl7dlaci/hpWJCd34EtjUlITnwH6g1NBMwiNnDDnrIbEHrbjFMSZ58h3VRLEL4NSZETEwlDvnkd3q9fQkjI4e/fzEAH/mCyYtKHyuOIMy16UbpdGnJoj1+GO+xjZskyCOLCWEg6gIX++szFe/PCNiMJ0uQL46qmSCgkFOM8qexgVJbbmQVB4WpwUsBesJV4VV4GMdwX6UeXtyp8uy8zdB3SJXsWUkA97VpUXHAFEoPBDou6sxD9mNJac7goR3Boj/Z3Oxx2w2dh0H7Yni7jUReLzGynTzs8VK13AX4j7nPcu+0WFsAp2820AWKOm4NPJw7YyFbQaRIWeu4Zb7IEMJAbQdvXSfWNVpWjePelQgQvwMKdQrd+bTBlDT5jE8YoHxviIuuGaIWD9ELqRtNuwGHbY6KuW4I2mCSVWP1Pz3Jf2+YpCRrfiE6ayKvynZ9oXL0YvGwXUp7AOft2xxzPBKueZm1YW2FcKhoMor4GfZjExyGY2U/l5Ltuoxt/kBDMOqH1dWT+xaxyb86AcvTjQwkAXftpmqAyzhQ1JCYiAYM3ApP1vkheOEAXeRr8+kIsOlMNAW0qU/uKtRIO0yDEBShBsJ4y3e5j17dMhtARpBWGbmFihWTKNJw8VMhwJ/zIxOBqWBQGEhKNA8caH42GD4azKchvYeGHuOuQ8ODjlj7IVBieAuz6pljHQjTulbnOv/TS3KNB211gGndHMCh8ixnWcZu0HQ6LcgzGO4Adr10qTDtQdhjEVCuE6F1TsfuqjHllWDciE5+HiMwzEyHHJwRwa5bya/Z6xiDcm6tKTHUE1yceoHJQHV461qDX2n+YuxdjGrpJbYnVYHz4alp1oftu+cMtzGk0HxoCRl6sMu1NVyBq0BHnGbIAnfPi6vBw6F+zoCBm4BLKMlyZ9ppW8iLvWFItJKzdnwWjD32Wj25TwqK+mvkDO9Dz2zE5PtVU9SK8/Xh0VvgNQpDrZp0KN7pswHHh7HHZJqzswbhdOEPG6ZgOkywi4sF+sgjc/5X0OZ6riLERY6ws92BnEz+WZQD/OF03t8TJ8h9y+CoZdvDo0gPnuyUYaFuQXuSZRrsFnRjgstJFgUY/rOzSCMgNLUHbKeE76mP8IqI1LUbQ+i2JAwzgIYzFC9Q/SiPpCXrwbKgi5q4u4aQPnTm0aCnBELeab2jCmegHB/O/FYzuGQnfmt/a6QhJrUC3A+Uj9CO7GSSk1USvzPme01vDnV5ceiXg8NlH9my0dzqtle3KPFIrQO9qHHfeirdG49B+j/3uBqTpv0BaqKV8oXM6HfHE+o6KOU6cXQeci/CbEPMXumxoMpb29KeywPU6aPUS5twDaXPKrm1PmVgwuDcl/obFlUMLXT6eajik/LAm3/n4gAUVC/yB4L1Y9HeBQ2HxGTOYr46BGUCHFbsw6HitioNHwfpgcaUtKPYMQkU9qDKI2ZAi2AquKcVEoIzn96enF5WejWu9W83GGMw8HREs9VMz3gV/6F0FmMBqcOCwuC1m4W3EO/JrUlqiuC3YyNFf0Y+qg38HjJJ3MB33sWz1w6j1BykuyywuNDCUkECEPnf31zf+C7T4GvT5EswwwpyAaynKDDfL4RIYbC8vYKd4gUkzpKUyBdZ+UzqSlXTU5yFJTRbD/ZK8xqD/RxEJyBDTWKN/Gujia/AHkyVcNB/3H/7EdfEEFmMK8nuJgPgC/aNLY0fgD4GfZWl6NBRmGp201NWKjyGi/zZ9oPNJu5jbb94+hx5ouAQSwnorEiWR32cE2LaAn63E1emoV6/RDnz4RyzBYtqV2oc/GUu8tuL9rsKkVsBIuBxrYDIeXB7GdT0a92aOpCrAvI2rtreAWfTBJhxSm5tBhSOBQAFm5dFeWq+Hds086Ug0EEqDMRl3VMUCUl+qC7L/GgvOmp7EE2Y2isBFkLyX4I3XueFnG1aQdoU1oSirsOPkGAFBxptfUCno3a/SIyw8wCI98Cn6kR0CuudohC/BQsP3MPhQDDi9vQCv4FgKYhcYxJ9hbNsJNeMfPVJ6rJFfFydmoTc0fIVFSPi20o2+OkOMQbnNYR0XyW27ZqckscC5NoOz4FZz0kNtgFy9DKrGZTRp980YVNZ/QelFAT9Ed7yHiIECRXjx9ALXQ7MKm0PQpTZIWi+jzyFRmi67wdCKfjRaIS8f6Jq3oaJsCM6677Gm28Og4epEtNee3sF4RCWhcnS/BCddo2C8XgGmMVDiwpiFmAUxclxOA0P/MWhwucwnn66F42h9Gj5O8AqiGsYUEmjIkEz56GcpyqxFn6dQPJbrIXCplh9egbIPQ2e/Oa249H2SXtDQQ1DXrgwGG0icTkpM4P9lxUFHiLi2fC2+lbIkqAc3gskthRy2SYWKbAg2FFrsZaibvhH7GWfaJO+dWRHpz4rn+wxjQ3zdCBo5oFV3pinN1ZFwQyD54rREn4IxYJqmRlVHQkzUNxnjsrA1ZkEolZOSX9WrG57EONEMbRfDIEkMDwbHBw3xeUBvXAZ1fYT9MSD9pwAwecz/6A7t/6eGW33vgWtvhpEsH8dqi4F4D4FDnM22FqvMzd6GOP0iHBKLkj+OeTQrrGZY4Ztxfl9jd0y0vgA1QeqEeBSBXlgwv7SWaTMsjI9RRiU4YDocDb66IGtXepH3p8gba+ZjhlWVl12B8PsUJ6aBdl/cb67nOt1QIEbSi1Hz/6rsR/ZfRWLiEnrMZWcWVJYcpI57cNpDg3QnCHgy7BpezINVZmb4T9gYNRXfSX0ZF3duQWuHo72nox6yqtcivpkeUU3Pd31op52qKKQi/oZQ4funJLa3cKpi7IGR0IRBG5rUpDAkifc4ejtdHDh2H4aI/t2DGztbHer9QlXUYhKbIdbjQ0es1CyC18qykqpC51L80ypMKv4giHwu2gxbC73HEe/17dX917gh6MDL3UQJ3ytV8xHhrC484XNAp6Vgmg+h/vHwTfYL24iOMfyAq9qDe+/PaqGK0RsHbChn1Qn2NOq8AqLqZGxgNHHwHRa+Hb//GnaO85lYRkNrO76PMEnPYGwetDUgHyva6x16dva67Vs8WBJ8j5Ti7TBGwJeDsdQS1eR59jx73DS4Fnlo/H9Gaoz98aEdXsbpdTGk/OlgvkvK6j2gL7OpNaIEc0+Ct/Ah1S0wV3BYzFuFgdyY3d01Erq/Bwt8PV3fbVHKkkBfoGLCd0pKb+NLq65vATGDdIcdj22+hYRwL1fFPt3gK5DxGY7d6Ppw7BbaEcXjJywFyIZzzFAdzoQBHvvO1lqnSBo9ZghnSl/+r9bmWGs4vus86pvSaAj5TZNo9WEz7puYmqpLqTsazImQBrEToiiuEEMFwS0+kVde512CNay1ZxXj035kMX+x/mDCqfB3Ea7WHHa0C/GZv3GAOaQpuB4bZxatkev/VN7u6c6D6NDBmg72KrwID3Ww2PcKHu5bq3VCcodkeeI72NxCLifPVQgJ41UY6G6DVJIh07vSh7hzJ8SyIHSKsdIQ2ZX447jiFIhT4LulQIRhkC6dU+C6G3prEYxKZMq8mu4qjDzOfzpER3PQX+dTNyBReKG7X27/BsJ328U49jgF4hToKgqYNgw7MtOmwWB4Iys7Xm5C8ngeVu7l9qfKuH5KpwAvcp5wKhkbJR5iMvhuwhVQa+hI9gaTAeFmJU9K/VmsryzJsnE/ToE4Bf59KRCVYVBz6eJN3SE+Hcf8P5cqCoC/hpV7vcLFNzhx+xbW1ktgy38AksNdsMLjHxTh25G4BAZGcSaYTU8wCljE2VuwzD9nv9Dy70uSeMviFIhTIBYFYjIMWYCkha+2eK8Bc7gejGA0jo+cMi+aj6MX3EmgT6TxlSoXr8dtFdGoFE+LU+DEpECbDMPeLfNxloGLW4buxhk/vtKFFwx0IQcfVsXFAc/Q4e7t/y5n5Pa2x+NxCsQpcHwU+B9qqcQdk8W5fAAAAABJRU5ErkJggg==');\r\n            background-size: cover;\r\n            background-position: 0 0;\r\n            height: 34px;\r\n            width:  268px;\r\n            display: block;\r\n            padding: .2em 1em;\r\n        }\r\n        </style>\r\n        <nav>\r\n            <a href=\"index.html\" id=\"nav-logo\"></a>\r\n        </nav>`;\r\n    }\r\n}\r\n\r\n\n\n//# sourceURL=webpack://rs-url-parser-front/./src/components/header.js?");

/***/ }),

/***/ "./src/components/url-parser.js":
/*!**************************************!*\
  !*** ./src/components/url-parser.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ UrlParserForm)\n/* harmony export */ });\n/* harmony import */ var rs_url_parser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rs-url-parser */ \"./node_modules/rs-url-parser/index.js\");\n\r\n\r\nclass UrlParserForm extends HTMLElement {\r\n    constructor() {\r\n        super();\r\n        this.attachShadow({ mode: \"open\" });\r\n    }\r\n\r\n    get instanceDefault() {\r\n        return this.getAttribute(\"instance-default\");\r\n    }\r\n\r\n    get formatDefault() {\r\n        return this.getAttribute(\"format-default\");\r\n    }\r\n\r\n    get title() {\r\n        return this.getAttribute(\"title\");\r\n    }\r\n\r\n    connectedCallback() {\r\n        this.render();\r\n        let form = this.shadowRoot.querySelector(\"#url-parser-form\");\r\n        form.addEventListener(\"submit\", e => this.submitFormHandler(e));\r\n    }\r\n\r\n    render() {\r\n        const template = this.createTemplate();\r\n        this.shadowRoot.appendChild(template.content.cloneNode(true));\r\n    }\r\n\r\n    disconnectedCallback() {\r\n        let form = this.shadowRoot.querySelector(\"#url-parser-form\");\r\n        form.removeEventListener(\"submit\");\r\n    }\r\n\r\n    resetForm(ele) {\r\n        ele.hidden = true;\r\n    }\r\n\r\n    submitFormHandler(e) {\r\n        e.preventDefault();\r\n        let parsed = this.shadowRoot.querySelector(\"#parsed\");\r\n        let format = this.shadowRoot.querySelector(\"#url-format\");\r\n        let instance = this.shadowRoot.querySelector(\"#url-instance\");\r\n        this.resetForm(parsed);\r\n        this.parseUrl(format.value, instance.value);\r\n    }\r\n\r\n    parseUrl(format, instance) {\r\n        let form = this.shadowRoot.querySelector(\"form\")\r\n        let inputs = form.querySelectorAll(\"input\");\r\n        if (!this.validate(inputs)) { return; }\r\n\r\n        let result = (0,rs_url_parser__WEBPACK_IMPORTED_MODULE_0__.urlParser)(format, instance);\r\n\r\n        let parsed = this.shadowRoot.querySelector(\"#parsed\");\r\n        parsed.hidden = false;\r\n        parsed.innerHTML = JSON.stringify(result, undefined, 3);\r\n    }\r\n\r\n    validate(inputs) {\r\n        let valid = true;\r\n        inputs.forEach(e => {\r\n            if (!e.value || !e.checkValidity()) {\r\n                valid = false;\r\n            }\r\n        });\r\n        return valid;\r\n    }\r\n\r\n    createTemplate() {\r\n        const template = document.createElement('template');\r\n        template.innerHTML = `\r\n        <link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU\" crossorigin=\"anonymous\">\r\n        <style>\r\n        input {\r\n            display: block;\r\n            width: 100%;\r\n            margin-bottom: 1em;\r\n        }\r\n\r\n        form {\r\n            padding: .6em;\r\n            text-align: left;\r\n        }\r\n\r\n        input[type=text] {\r\n            border-color: #f3f3f3;\r\n            background-color: #f3f3f3;\r\n        }\r\n\r\n        button[type=submit] {\r\n            margin: 0 auto;\r\n        }\r\n\r\n        code {\r\n            margin: 1em auto;\r\n            text-align: left;\r\n            background-color: #f3f3f3;\r\n        }\r\n\r\n        pre {\r\n            padding: .8em;\r\n            margin: 0;\r\n        }\r\n\r\n        #error {\r\n            max-width: 600px;\r\n            margin: 0 auto;\r\n        }\r\n        </style>\r\n        <div class=\"container\">\r\n        <h1>${this.title}</h1>\r\n        <div class=\"row row-cols-1 align-items-stretch g-4 py-5\">\r\n            <div class=\"col mx-auto\">\r\n                <div class=\"card card-cover h-100 overflow-hidden rounded-5 shadow-lg\">\r\n                    <form action=\"#\" id=\"url-parser-form\">\r\n                        <div class=\"form-floating\">\r\n                            <input type=\"text\" name=\"url_format\" id=\"url-format\" class=\"form-control\" required value=\"${this.formatDefault}\" placeholder=\"/:version/api/:collection/:id\"/>\r\n                            <label for=\"url-format\">URL Format</label>\r\n                        </div>\r\n                        <div class=\"form-floating\">\r\n                            <input type=\"text\" name=\"url_format\" id=\"url-instance\" class=\"form-control\" required value=\"${this.instanceDefault}\" placeholder=\"/6/api/listings/3?sort=desc&limit=10\"/>\r\n                            <label for=\"url-instance\">URL Instance</label>\r\n                        </div>\r\n                        <div class=\"text-center\">\r\n                            <button type=\"submit\" class=\"btn btn-primary my-2\">Parse Url</button>\r\n                        </div>\r\n                    </form>\r\n                    <div class=\"alert alert-danger\" role=\"alert\" id=\"error\" hidden></div>\r\n                    <code>\r\n                        <pre id=\"parsed\" hidden></pre>\r\n                    </code>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        </div>`;\r\n        return template;\r\n    }\r\n}\r\n\n\n//# sourceURL=webpack://rs-url-parser-front/./src/components/url-parser.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _components_header_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/header.js */ \"./src/components/header.js\");\n/* harmony import */ var _components_url_parser_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/url-parser.js */ \"./src/components/url-parser.js\");\n\r\n\r\n\r\nwindow.customElements.define(\"header-menu\", _components_header_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"]);\r\nwindow.customElements.define(\"url-parser-form\", _components_url_parser_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"]);\r\n\n\n//# sourceURL=webpack://rs-url-parser-front/./src/index.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/index.js");
/******/ 	
/******/ })()
;